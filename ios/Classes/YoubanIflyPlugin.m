#import "YoubanIflyPlugin.h"
#import "../../example/ios/Runner/iFly.h"

@implementation YoubanIflyPlugin
iFly *fly;
FlutterMethodChannel* channel;
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
    //init message channele
    channel = [FlutterMethodChannel
               methodChannelWithName:@"youban_ifly"
               binaryMessenger:[registrar messenger]];
    YoubanIflyPlugin* instance = [[YoubanIflyPlugin alloc] init];
    [registrar addMethodCallDelegate:instance channel:channel];
    //init fly
    fly =  [[iFly alloc] init];
    [fly initIse];
    //init eventchannel
    FLTSentenceRecognizeStreamHandler* sentenceRecognizeStreamHandler = [[FLTSentenceRecognizeStreamHandler alloc] init];
    FlutterEventChannel* sentenceRecognizeChannel =
    [FlutterEventChannel eventChannelWithName:@"plugins.flutter.io/sensors/gyroscope"
                              binaryMessenger:[registrar messenger]];
    [sentenceRecognizeChannel setStreamHandler:sentenceRecognizeStreamHandler];

}

- (void)handleMethodCall:(FlutterMethodCall*)call result:(FlutterResult)result {

    if ([@"getPlatformVersion" isEqualToString:call.method]) {
        result([@"iOS " stringByAppendingString:[[UIDevice currentDevice] systemVersion]]);
    } else if ([@"startRecord" isEqualToString:call.method]) {
        NSLog(@"starttttttrocord");
        [fly startRecord:call.arguments[@"sourceText"]];
        
        result([@"iOS " stringByAppendingString:[[UIDevice currentDevice] systemVersion]]);
    } else if ([@"stopRecord" isEqualToString:call.method]) {
        [fly stopRecord];
        NSLog(@"stopRecordstopRecord");
        result([@"iOS " stringByAppendingString:[[UIDevice currentDevice] systemVersion]]);
    }else if ([@"recognizeWithAudio" isEqualToString:call.method]) {
    NSString * path = call.arguments[@"path"];
    NSString * sourceText = call.arguments[@"sourceText"];
        [fly recognizeWithAudio :sourceText :path ];
             result([@"iOS " stringByAppendingString:[[UIDevice currentDevice] systemVersion]]);
         } else {
        result(FlutterMethodNotImplemented);
    }
}



@end



@implementation FLTSentenceRecognizeStreamHandler

- (FlutterError*)onListenWithArguments:(id)arguments eventSink:(FlutterEventSink)eventSink {
    [fly addRecognizeResultHandler:^(NSString * _Nonnull result) {
        eventSink(result);
    }];
    return nil;
}

- (FlutterError*)onCancelWithArguments:(id)arguments {
    return nil;
}

@end

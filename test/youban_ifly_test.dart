import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:youban_ifly/youban_ifly.dart';

void main() {
  const MethodChannel channel = MethodChannel('youban_ifly');

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return '42';
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('getPlatformVersion', () async {
    expect(await YoubanIfly.platformVersion, '42');
  });
}

//
//  iFly.h
//  Runner
//
//  Created by sunxy on 2019/6/26.
//  Copyright © 2019 The Chromium Authors. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ISEParams.h"
#import "YoubanIflyPlugin.h"

NS_ASSUME_NONNULL_BEGIN

typedef void (^completionHandler)(NSString* result);
@interface iFly : NSObject
@property (nonatomic, strong) ISEParams *iseParams;
- (void)initIse;
- (void)stopRecord;
- (void)startRecord:(NSString*)sourceText;
-(void) addRecognizeResultHandler:(completionHandler)handler;
-(void) recognizeWithAudio:(NSString*)sourceText :(NSString*)_pcmFilePath;
@end

NS_ASSUME_NONNULL_END

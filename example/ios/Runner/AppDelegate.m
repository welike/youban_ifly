#include "AppDelegate.h"
#include "GeneratedPluginRegistrant.h"
#import "IFlyMSC/IFlyMSC.h"
#import "Definition.h"

@interface AppDelegate () <FlutterStreamHandler>

@property (nonatomic, copy)   FlutterEventSink     eventSink;

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application
    didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
  [GeneratedPluginRegistrant registerWithRegistry:self];
    
    

    
    
  // Override point for customization after application launch.
    //Set log level
    [IFlySetting setLogFile:LVL_ALL];
    //Set whether to output log messages in Xcode console
    [IFlySetting showLogcat:YES];
    
    //Set the local storage path of SDK
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cachePath = [paths objectAtIndex:0];
    [IFlySetting setLogFilePath:cachePath];
    
    //Set APPID
    NSString *initString = [[NSString alloc] initWithFormat:@"appid=%@",APPID_VALUE];
    
    //Configure and initialize iflytek services.(This interface must been invoked in application:didFinishLaunchingWithOptions:)
    [IFlySpeechUtility createUtility:initString];
  return [super application:application didFinishLaunchingWithOptions:launchOptions];
}


- (FlutterError*)onListenWithArguments:(id)arguments eventSink:(FlutterEventSink)eventSink {
    self.eventSink = eventSink;
    
    return nil;
}

- (FlutterError*)onCancelWithArguments:(id)arguments {

    self.eventSink = nil;
    return nil;
}

@end

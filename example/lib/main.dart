import 'package:flutter/material.dart';
import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:youban_ifly/youban_ifly.dart';

void main() => runApp(MaterialApp(debugShowCheckedModeBanner: false, home: MyApp()));

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  // Create a client transformer
  List<StreamSubscription<dynamic>> _streamSubscriptions =
  <StreamSubscription<dynamic>>[];
  @override
  void initState() {
    super.initState();

    _streamSubscriptions.add(gyroscopeEvents.listen((String result) {
     print(result);
    }));

  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Center(
          child: Column(children: <Widget>[
            FlatButton(onPressed: (){
              startRecord();
            },child: Text('开始录音'),),
            FlatButton(onPressed: (){
              stopRecord();
            },child: Text('停止录音'),),
            FlatButton(onPressed: (){

            },child: Text('录音测评'),),
          ],),
        ),

    );
  }

}
